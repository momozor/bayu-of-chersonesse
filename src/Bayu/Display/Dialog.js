/*
  This file is part of Bayu of Chersonesse.
  Copyright (C) 2019 by Momozor <skelic3@gmail.com>
  Copyright (C) 2019 by Teoh Han Hui <teohhanhui@gmail.com>

  Bayu of Chersonesse is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Bayu of Chersonesse is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { game } from '../../Game.js';
import { DEFAULT_FONT_NAME } from '../../GlobalConstants.js';

function addDialog(x, y, dialogFrame) {
    let dialogBox;
    const onClick = new Promise((resolve) => {
        dialogBox = game.add.button(x, y, dialogFrame, () => {
            resolve();
        });
    });
    return { dialogBox, onClick };
}

export class Dialog {
    display(playerName, text, dialogFrame = 'dialogBoxBlue') {
        const defaultXPos = 0;
        const dialogBoxYPos = 450;
        const { dialogBox, onClick } = addDialog(defaultXPos, dialogBoxYPos, dialogFrame);
        this.dialogBox = dialogBox;
        const playerLabelXPos = 20;
        const playerLabelYPos = 1;
        const playerLabel = game.add.bitmapText(playerLabelXPos, playerLabelYPos, DEFAULT_FONT_NAME, playerName);
        const textStoryXPos = 30;
        const textStoryYPos = 30;
        const textStory = game.add.bitmapText(textStoryXPos, textStoryYPos, DEFAULT_FONT_NAME, text);
        textStory.maxWidth = 800;
        this.dialogBox.fixedToCamera = true;
        this.dialogBox.addChild(playerLabel);
        this.dialogBox.addChild(textStory);
        return onClick;
    }

    delete() {
        this.dialogBox.destroy();
    }
}
