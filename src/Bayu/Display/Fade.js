/*
  This file is part of Bayu of Chersonesse.
  Copyright (C) 2019 by Momozor <skelic3@gmail.com>

  Bayu of Chersonesse is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Bayu of Chersonesse is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { game } from '../../Game.js';

/**
 * Fade the game screen with black color.
 *
 * @param seconds How many seconds until fade is complete.
 */
export function fadeToBlack(seconds) {
    const miliseconds = 1000;
    const blackScreen = 0x000000;
    game.camera.fade(blackScreen, (miliseconds * seconds));
}

/**
 * Run a new state/scene when fade process is complete.
 *
 * @param stateName State or scene name to start.
 * See StateEntries.[tj]s file for list of states.
 */
export function changeToStateOnFadeDone(stateName) {
    game.camera.onFadeComplete.add(function () {
        game.state.start(stateName);
    }, this);
}
