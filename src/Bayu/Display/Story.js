/*
  This file is part of Bayu of Chersonesse.
  Copyright (C) 2019 by Momozor <skelic3@gmail.com>

  Bayu of Chersonesse is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Bayu of Chersonesse is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { game } from '../../Game.js';
import { SCREEN_MAX_WIDTH, DEFAULT_FONT_NAME } from '../../GlobalConstants.js';

export class Story {
    constructor(storyName) {
        this.storyName = storyName;
    }

    loadStory() {
        game.load.json(this.storyName, `static/assets/stories/${this.storyName}.json`);
    }

    rollStory() {
        const storyText = game.cache.getJSON(this.storyName).story;
        const wordDelay = game.cache.getJSON(this.storyName).wordDelay;
        const lineDelay = game.cache.getJSON(this.storyName).lineDelay;
        const defaultXPos = 0;
        const defaultYPos = 0;
        const textRenderer = game.add.bitmapText(defaultXPos, defaultYPos, DEFAULT_FONT_NAME, '');
        textRenderer.maxWidth = SCREEN_MAX_WIDTH;
        const storyTraverse = new StoryTraverse(storyText, textRenderer, wordDelay, lineDelay);
        storyTraverse.beginTraverseLine();
    }
}

class StoryTraverse {
    constructor(storyText, textRenderer, wordDelay, lineDelay) {
        this.line = [];
        this.wordIndex = 0;
        this.lineIndex = 0;
        this.storyText = storyText;
        this.textRenderer = textRenderer;
        this.wordDelay = wordDelay;
        this.lineDelay = lineDelay;
    }

    beginTraverseLine() {
        if (this.lineIndex === this.storyText.length) {
            return;
        }
        this.line = this.storyText[this.lineIndex].split(' ');
        this.wordIndex = 0;
        game.time.events.repeat(this.wordDelay, this.line.length, this.nextWord, this);
        this.lineIndex++;
    }

    nextWord() {
        this.textRenderer.text = this.textRenderer.text.concat(this.line[this.wordIndex] + ' ');
        this.wordIndex++;
        if (this.wordIndex === this.line.length) {
            this.textRenderer.text = this.textRenderer.text.concat('\n');
            game.time.events.add(this.lineDelay, this.beginTraverseLine, this);
        }
    }
}
