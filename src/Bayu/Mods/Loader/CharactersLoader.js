/*
  This file is part of Bayu of Chersonesse.
  Copyright (C) 2019 by Momozor <skelic3@gmail.com>

  Bayu of Chersonesse is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Bayu of Chersonesse is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { game } from '../../../Game.js';
import { MODS, MODS_PATH } from '../../../GlobalConstants.js';

export class CharactersLoader {
    constructor() {
        this.characterList = game.cache.getJSON(MODS).characters;
    }

    loadJSON() {
        this.characterList.map((character) => {
            const placesRootPath = `${MODS_PATH}/characters/${character}`;
            game.load.json(`character-${character}`, `${placesRootPath}/character.json`);
        });
    }

    loadAssets() {
        // temporary hack, please
        // make a animatuon of 32x32 per frame!!!
        const pixelWidth = 41;
        const pixelHeight = 36;
        this.characterList.map((character) => {
            const placesRootPath = `${MODS_PATH}/characters/${character}`;
            const characterSpritePath = game.cache.getJSON(`character-${character}`).sprite;
            game.load.spritesheet(`character-${character}-sprite`, `${placesRootPath}/${characterSpritePath}`, pixelWidth, pixelHeight);
        });
    }
}
