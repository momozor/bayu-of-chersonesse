/*
  This file is part of Bayu of Chersonesse.
  Copyright (C) 2019 by Momozor <skelic3@gmail.com>

  Bayu of Chersonesse is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Bayu of Chersonesse is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { game } from '../../../Game.js';
import { MODS, MODS_PATH } from '../../../GlobalConstants.js';

export class PlacesLoader {
    constructor() {
        this.placeList = game.cache.getJSON(MODS).places;
    }
    
    loadJSON() {
        this.placeList.map((place) => {
            const placesRootPath = `${MODS_PATH}/places/${place}`;
            game.load.json(`place-${place}`, `${placesRootPath}/place.json`);
        });
    }

    loadAssets() {
        this.placeList.map((place) => {
            const texturesPath = game.cache.getJSON(`place-${place}`).textures;
            const mapPath = game.cache.getJSON(`place-${place}`).map;
            const placesRootPath = `${MODS_PATH}/places/${place}`;
            game.load.tilemap(`place-${place}-map`, `${placesRootPath}/${mapPath}`, null, Phaser.Tilemap.TILED_JSON);
            game.load.image(`place-${place}-textures`, `${placesRootPath}/${texturesPath}`);
        });
    }
}
