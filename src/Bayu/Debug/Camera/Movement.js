/*
  This file is part of Bayu of Chersonesse.
  Copyright (C) 2019 by Momozor <skelic3@gmail.com>

  Bayu of Chersonesse is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Bayu of Chersonesse is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { game } from '../../../Game.js';

export class Movement {
    makeMovable() {
        const movementSpeed = 4;
        const arrows = game.input.keyboard.createCursorKeys();
        if (arrows.up.isDown) {
            game.camera.y -= movementSpeed;
        }
        else if (arrows.down.isDown) {
            game.camera.y += movementSpeed;
        }
        else if (arrows.left.isDown) {
            game.camera.x -= movementSpeed;
        }
        else if (arrows.right.isDown) {
            game.camera.x += movementSpeed;
        }
    }
}
