/*
  This file is part of Bayu of Chersonesse.
  Copyright (C) 2019 by Momozor <skelic3@gmail.com>

  Bayu of Chersonesse is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Bayu of Chersonesse is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { game } from '../../../Game.js';
import { makeCameraFollowCharacter as cameraFollowCharacter } from '../Camera/Movement.js';
/**
 * Set character walking animations.
 * All exported animation keys for the character are:
 * idle, up, down, left, right.
 *
 * @parameter character Character object.
 * @parameter animations Animations object.
 * @parameter frameChangingspeed How fast the frame changing to another.
 */
function setCharacterWalkingAnimations(character, animations, frameChangingSpeed = 10) {
    character.animations.add('idle', animations.idle, null, null);
    character.animations.add('up', animations.up, frameChangingSpeed);
    character.animations.add('down', animations.down, frameChangingSpeed, true);
    character.animations.add('left', animations.left, frameChangingSpeed, true);
    character.animations.add('right', animations.right, frameChangingSpeed, true);
}
function walkDirectionNorth(character, movementSpeed) {
    character.body.velocity.x = 0;
    character.body.velocity.y = -movementSpeed;
    character.animations.play('up');
}
function walkDirectionSouth(character, movementSpeed) {
    character.body.velocity.x = 0;
    character.body.velocity.y = movementSpeed;
    character.animations.play('down');
}
function walkDirectionWest(character, movementSpeed) {
    character.body.velocity.y = 0;
    character.body.velocity.x = -movementSpeed;
    character.animations.play('left');
}
function walkDirectionEast(character, movementSpeed) {
    character.body.velocity.y = 0;
    character.body.velocity.x = movementSpeed;
    character.animations.play('right');
}
function walkIdle(character) {
    character.body.velocity.y = 0;
    character.body.velocity.x = 0;
    character.animations.play('idle');
}
export class Movement {
    constructor(character) {
        this.character = character;
    }
    enableMovement(characterJSONKey) {
        cameraFollowCharacter(this.character);
        const animations = game.cache.getJSON(characterJSONKey).animations;
        setCharacterWalkingAnimations(this.character, animations);
        this.key = game.input.keyboard;
        this.arrow = this.key.createCursorKeys();
    }
    makeMovableDesktop(movementSpeed) {
        if (this.key.isDown(Phaser.Keyboard.W) || this.arrow.up.isDown) {
            walkDirectionNorth(this.character, movementSpeed);
        }
        else if (this.key.isDown(Phaser.Keyboard.S) || this.arrow.down.isDown) {
            walkDirectionSouth(this.character, movementSpeed);
        }
        else if (this.key.isDown(Phaser.Keyboard.A) || this.arrow.left.isDown) {
            walkDirectionWest(this.character, movementSpeed);
        }
        else if (this.key.isDown(Phaser.Keyboard.D) || this.arrow.right.isDown) {
            walkDirectionEast(this.character, movementSpeed);
        }
        else {
            walkIdle(this.character);
        }
    }
    makeMovable(movementSpeed = 200) {
        this.makeMovableDesktop(movementSpeed);
    }
}
