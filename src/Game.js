import { SCREEN_MAX_WIDTH, SCREEN_MAX_HEIGHT } from './GlobalConstants.js';

const game = new Phaser.Game(SCREEN_MAX_WIDTH, SCREEN_MAX_HEIGHT, Phaser.AUTO, 'canvas');
export { game };
