/*
  This file is part of Bayu of Chersonesse.
  Copyright (C) 2019 by Momozor <skelic3@gmail.com>

  Bayu of Chersonesse is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Bayu of Chersonesse is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { game } from './Game.js';
import { DEFAULT_FONT_NAME } from './GlobalConstants.js';
import { Story } from './Bayu/Display/Story.js';
import * as text from './Bayu/Display/Text.js';

export class Credits {
    constructor() {
        this.bayuCredits = new Story('developersCredits');
    }
    
    preload() {
        this.bayuCredits.loadStory();
    }
    
    create() {
        this.bayuCredits.rollStory();
        const backToMenuButtonYPos = 500;
        const backToMenuButton = game.add.button(game.world.centerX, backToMenuButtonYPos, 'continueButtonUp', () => {
            game.state.start('menu');
        });
        const defaultXPos = 0;
        const defaultYPos = 0;
        const backLabel = game.add.bitmapText(defaultXPos, defaultYPos, DEFAULT_FONT_NAME, 'Back');
        backToMenuButton.addChild(backLabel);
        text.centerTextWithinButton(backLabel, backToMenuButton);
    }
}
