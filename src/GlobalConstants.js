/*
  This file is part of Bayu of Chersonesse.
  Copyright (C) 2019 by Momozor <skelic3@gmail.com>

  Bayu of Chersonesse is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Bayu of Chersonesse is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

// Global game configuration constants.
const VERSION = '0.2.0';
const SCREEN_MAX_WIDTH = 800;
const SCREEN_MAX_HEIGHT = 600;
const DOMAIN = `${location.protocol}/${location.hostname}`;
const PORT = location.port;
const DNS = DOMAIN + (PORT ? `:${PORT}` : '');
const DEFAULT_FONT_NAME = 'ipanema-secco';
const MODS = 'mods';
const MODS_PATH = 'static/assets/mods';

// Database table names.
const MAIN_PLAYER_DATA = 'mainPlayerData';
export {
    DOMAIN,
    PORT,
    DNS,
    DEFAULT_FONT_NAME,
    MODS,
    MODS_PATH,
    SCREEN_MAX_WIDTH,
    SCREEN_MAX_HEIGHT,
    VERSION,
    MAIN_PLAYER_DATA
};
