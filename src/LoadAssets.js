/*
  This file is part of Bayu of Chersonesse.
  Copyright (C) 2019 by Momozor <skelic3@gmail.com>

  Bayu of Chersonesse is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Bayu of Chersonesse is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { CharactersLoader } from './Bayu/Mods/Loader/CharactersLoader.js';
import { PlacesLoader } from './Bayu/Mods/Loader/PlacesLoader.js';
import { AssetsLoader } from './Bayu/Mods/Loader/AssetsLoader.js';
import { game } from './Game.js';
import * as text from './Bayu/Display/Text.js';
import { DEFAULT_FONT_NAME } from './GlobalConstants.js';

const assetsLoaderLabelYPos = 50;
const defaultXPos = 0;
const defaultYPos = 0;

export class LoadAssets {
    preload() {
        game.add.bitmapText(defaultXPos, defaultYPos, DEFAULT_FONT_NAME, 'Global settings loaded.');
        this.assetsLoaderLabel = game.add.bitmapText(
            defaultXPos,
            assetsLoaderLabelYPos,
            DEFAULT_FONT_NAME,
            'Loading assets...'
        );
        const bayuLoader = new AssetsLoader();
        bayuLoader.loadAssets(new CharactersLoader());
        bayuLoader.loadAssets(new PlacesLoader());
    }
    
    create() {
        this.assetsLoaderLabel.destroy();
        const fromTheRightOffset = 500;
        game.add.bitmapText(defaultXPos, assetsLoaderLabelYPos, DEFAULT_FONT_NAME, 'Assets loaded.');
        const continueToMenuButton = game.add.button(game.world.centerX, fromTheRightOffset, 'continueButtonUp', () => {
            game.state.start('menu');
        });
        const continueToMenuLabel = game.add.bitmapText(defaultXPos, 0, DEFAULT_FONT_NAME, 'Continue');
        continueToMenuButton.addChild(continueToMenuLabel);
        text.centerTextWithinButton(continueToMenuLabel, continueToMenuButton);
    }
}
