/*
  This file is part of Bayu of Chersonesse.
  Copyright (C) 2019 by Momozor <skelic3@gmail.com>

  Bayu of Chersonesse is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Bayu of Chersonesse is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { game } from './Game.js';
import { Boot } from './Boot.js';
import { LoadGlobals } from './LoadGlobals.js';
import { Inventory } from './Inventory.js';
import { LoadAssets } from './LoadAssets.js';
import { Menu } from './Menu.js';
import { Credits } from './Credits.js';
import { StoryOne } from './Stories/StoryOne.js';
import { PlaceRusaTavern } from './Places/PlaceRusaTavern.js';
import { PlaceRaznokWest } from './Places/PlaceRaznokWest.js';
// Register all game scenes here.
game.state.add('boot', Boot);
game.state.add('loadGlobals', LoadGlobals);
game.state.add('inventory', Inventory);
game.state.add('loadAssets', LoadAssets);
game.state.add('menu', Menu);
game.state.add('credits', Credits);
game.state.add('storyOne', StoryOne);
game.state.add('placeRusaTavern', PlaceRusaTavern);
game.state.add('placeRaznokWest', PlaceRaznokWest);
// Entry point.
game.state.start('boot');
