/*
  This file is part of Bayu of Chersonesse.
  Copyright (C) 2019 by Momozor <skelic3@gmail.com>
  Copyright (C) 2019 by Teoh Han Hui <teohhanhui@gmail.com>

  Bayu of Chersonesse is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Bayu of Chersonesse is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { game } from '../Game.js';
import { Movement as CharacterMovement } from '../Bayu/Mechanism/Character/Movement.js';
import { Dialog } from '../Bayu/Display/Dialog.js';
import { MAIN_PLAYER_DATA } from '../GlobalConstants.js';
import localforage from 'localforage';
import * as utility from '../Bayu/Mods/Utilities/Places.js';

export class PlaceRusaTavern {
    create() {
        this.map = game.add.tilemap('place-rusa-tavern-map');
        this.map.addTilesetImage('rusa-tavern-textures', 'place-rusa-tavern-textures');
        this.backgroundLayer = this.map.createLayer('background-layer');
        this.furnitureLayer = this.map.createLayer('furniture-layer');
        this.decorationLayer = this.map.createLayer('decoration-layer');
        this.collisionLayer = this.map.createLayer('collision-layer');
        utility.enableScrollDeltaForAllLayers([
            this.backgroundLayer,
            this.furnitureLayer,
            this.decorationLayer,
            this.collisionLayer
        ]);
        this.collisionLayer.visible = false;
        this.placeRaznokWestDoor = game.add.group();
        this.placeRaznokWestDoor.enableBody = true;
        const groupID = 61;
        const frame = 1;
        this.map.createFromObjects(
            'place-raznok-west-door-layer',
            groupID,
            'place-rusa-tavern-textures',
            frame,
            true,
            false,
            this.placeRaznokWestDoor
        );
        const fromMinID = 1;
        const toMaxID = 100000;
        this.map.setCollisionBetween(fromMinID, toMaxID, true, 'collision-layer');
        this.backgroundLayer.resizeWorld();
        this.mainPlayer = game.add.sprite(
            game.world.centerX,
            game.world.centerY,
            'character-biru-sprite'
        );
        game.physics.enable(this.mainPlayer, Phaser.Physics.ARCADE);
        this.mainPlayer.body.collideWorldBounds = true;
        this.bayuCharacterMovement = new CharacterMovement(this.mainPlayer);
        this.bayuCharacterMovement.enableMovement('character-biru');

        async function getMainPlayerData() {
            return localforage.getItem(MAIN_PLAYER_DATA);
        }
        
        getMainPlayerData().then((value) => {
            if (value.passedRusaTavern === false) {
                localforage.setItem(MAIN_PLAYER_DATA, {
                    passedRusaTavern: true
                });
                const dialogBox = new Dialog();
                dialogBox.display('You', 'Ugh...my head hurts...')
                    .then(() => dialogBox.delete())
                    .then(() => dialogBox.display('You', 'Where....am I?'))
                    .then(() => dialogBox.delete())
                    .then(() => dialogBox.display('Mysterious Figure', 'Nevermind me. Who are you?'))
                    .then(() => dialogBox.delete())
                    .then(() => dialogBox.display('You', 'Bleh'))
                    .then(() => dialogBox.delete());
            }
        });
    }
    
    update() {
        game.physics.arcade.collide(this.mainPlayer, this.collisionLayer);
        const goToPlaceRaznokWest = () => {
            game.state.start('placeRaznokWest');
        };
        game.physics.arcade.overlap(this.mainPlayer, this.placeRaznokWestDoor, goToPlaceRaznokWest);
        this.bayuCharacterMovement.makeMovable();
    } 
}
