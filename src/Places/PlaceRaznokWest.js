/*
  This file is part of Bayu of Chersonesse.
  Copyright (C) 2019 by Momozor <skelic3@gmail.com>

  Bayu of Chersonesse is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Bayu of Chersonesse is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { game } from '../Game.js';
import { Movement as CharacterMovement } from '../Bayu/Mechanism/Character/Movement.js';
import * as utility from '../Bayu/Mods/Utilities/Places.js';

export class PlaceRaznokWest {
    create() {
        this.map = game.add.tilemap('place-raznok-west-map');
        this.map.addTilesetImage('raznok-west-textures', 'place-raznok-west-textures');
        this.backgroundLayer = this.map.createLayer('background-layer');
        
        // Spawn player after backround layer has created. 
        this.mainPlayer = game.add.sprite(game.world.centerX, game.world.centerY, 'character-biru-sprite');
        this.decorationLayer = this.map.createLayer('decoration-layer');
        this.decorationTwoLayer = this.map.createLayer('decoration-two-layer');
        this.collisionLayer = this.map.createLayer('collision-layer');
        utility.enableScrollDeltaForAllLayers([
            this.backgroundLayer,
            this.decorationLayer,
            this.decorationLayer,
            this.collisionLayer
        ]);
        this.placeRusaTavernDoor = game.add.group();
        this.placeRusaTavernDoor.enableBody = true;
        const groupID = 143;
        const frame = 1;
        this.map.createFromObjects(
            'place-rusa-tavern-door-layer',
            groupID,
            'place-raznok-west-textures',
            frame,
            true,
            false,
            this.placeRusaTavernDoor
        );
        
        this.collisionLayer.visible = false;
        const fromMinID = 1;
        const toMaxID = 100000;
        this.map.setCollisionBetween(fromMinID, toMaxID, true, 'collision-layer');
        this.backgroundLayer.resizeWorld();
        game.physics.enable(this.mainPlayer, Phaser.Physics.ARCADE);
        this.mainPlayer.body.collideWorldBounds = true;
        this.bayuCharacterMovement = new CharacterMovement(this.mainPlayer);
        this.bayuCharacterMovement.enableMovement('character-biru');
    }
    
    update() {
        game.physics.arcade.collide(this.mainPlayer, this.collisionLayer);
        const goToPlaceRusaTavern = () => {
            game.state.start('placeRusaTavern');
        };
        game.physics.arcade.overlap(this.mainPlayer, this.placeRusaTavernDoor, goToPlaceRusaTavern);
        this.bayuCharacterMovement.makeMovable();
    }
}
