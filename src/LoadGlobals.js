/*
  This file is part of Bayu of Chersonesse.
  Copyright (C) 2019 by Momozor <skelic3@gmail.com>

  Bayu of Chersonesse is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Bayu of Chersonesse is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
import { game } from './Game.js';
import { DEFAULT_FONT_NAME } from './GlobalConstants.js';
import { AssetsLoader } from './Bayu/Mods/Loader/AssetsLoader.js';
import { ItemsLoader } from './Bayu/Mods/Loader/ItemsLoader.js';
import { CharactersLoader } from './Bayu/Mods/Loader/CharactersLoader.js';
import { PlacesLoader } from './Bayu/Mods/Loader/PlacesLoader.js';

/**
 * Preload all _GLOBAL_ game assets that aren't specific for Mods.
 *
 * Load games assets that are basically not covered in Mods
 * paths, whether items, characters, or places.
 */
export class LoadGlobals {
    preload() {
        const xPos = 0;
        const yPos = 0;
        game.add.bitmapText(xPos, yPos, DEFAULT_FONT_NAME, 'Loading global settings...');
        game.load.image('continueButtonUp', 'static/assets/buttons/continueButtonUp.png');
        game.load.image('menuBackgroundMimsi', 'static/assets/backgrounds/mimsi.png');
        game.load.image('dialogBoxBlue', 'static/assets/dialog-boxes/dialogBoxBlue.png');
        game.load.audio('musicBonfire', 'static/assets/music/bonfire.mp3');
        const bayuLoader = new AssetsLoader();
        bayuLoader.loadJSON(new ItemsLoader());
        bayuLoader.loadJSON(new CharactersLoader());
        bayuLoader.loadJSON(new PlacesLoader());
    }
    create() {
        // phaser-input type definitions are broken?
        // @ts-ignore
        game.add.plugin(PhaserInput.Plugin);
        game.state.start('loadAssets');
    }
}
