/*
  This file is part of Bayu of Chersonesse.
  Copyright (C) 2019 by Momozor <skelic3@gmail.com>

  Bayu of Chersonesse is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Bayu of Chersonesse is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

import { DEFAULT_FONT_NAME, SCREEN_MAX_WIDTH, SCREEN_MAX_HEIGHT, VERSION } from './GlobalConstants.js';
import { game } from './Game.js';
import * as fade from './Bayu/Display/Fade.js';
import * as text from './Bayu/Display/Text.js';

/**
 * Game top-level menu screen.
 *
 * By default, starts storyOne scene if
 * 'Start' button is clicked.
 *
 * This is the scene that hosts three states.
 * 1. Start
 * 2. Settings
 * 3. Credits
 */
export class Menu {
    create() {
        const backgroundMusic = game.add.audio('musicBonfire');
        const defaultXPos = 0;
        const defaultYPos = 0;
        const secondsToComplete = 2;
        const startButtonYPos = 310;
        const creditsButtonYPos = 230;
        const xPosFromLeftOffset = 80;
        const versionLabelYPos = 500;
        const mimsiBackground = game.add.tileSprite(
            defaultXPos,
            defaultYPos,
            SCREEN_MAX_WIDTH,
            SCREEN_MAX_HEIGHT,
            'menuBackgroundMimsi'
        );
        // Start background music (infinitely looped);
        const volume = 0.6;
        backgroundMusic.loopFull(volume);
        const startButton = game.add.button(xPosFromLeftOffset, startButtonYPos, 'continueButtonUp', () => {
            backgroundMusic.stop();
            fade.fadeToBlack(secondsToComplete);
        });
        const startLabel = game.add.bitmapText(defaultXPos, defaultYPos, DEFAULT_FONT_NAME, 'Start');
        startButton.addChild(startLabel);
        text.centerTextWithinButton(startLabel, startButton);
        fade.changeToStateOnFadeDone('storyOne');
        const creditsButton = game.add.button(xPosFromLeftOffset, creditsButtonYPos, 'continueButtonUp', () => {
            backgroundMusic.stop();
            game.state.start('credits');
        });
        const creditsLabel = game.add.bitmapText(defaultXPos, defaultYPos, DEFAULT_FONT_NAME, 'Credits');
        creditsButton.addChild(creditsLabel);
        text.centerTextWithinButton(creditsLabel, creditsButton);
        mimsiBackground.visible;
        game.add.bitmapText(xPosFromLeftOffset, versionLabelYPos, DEFAULT_FONT_NAME, `v${VERSION}`);
    }
}
