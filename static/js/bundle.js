(function (localforage) {
  'use strict';

  localforage = localforage && localforage.hasOwnProperty('default') ? localforage['default'] : localforage;

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  // Global game configuration constants.
  const VERSION = '0.2.0';
  const SCREEN_MAX_WIDTH = 800;
  const SCREEN_MAX_HEIGHT = 600;
  const DEFAULT_FONT_NAME = 'ipanema-secco';
  const MODS = 'mods';
  const MODS_PATH = 'static/assets/mods';

  // Database table names.
  const MAIN_PLAYER_DATA = 'mainPlayerData';

  const game = new Phaser.Game(SCREEN_MAX_WIDTH, SCREEN_MAX_HEIGHT, Phaser.AUTO, 'canvas');

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  /**
   * Pre-boot state.
   *
   * Loads JSON's Mods data and default font (ipanema secco)
   * to be used in [[Load]] for loading text label and dynamic
   *  assets.
   */
  class Boot {
      preload() {
          game.load.json(MODS, `${MODS_PATH}/mods.json`);
          game.load.bitmapFont(
              DEFAULT_FONT_NAME,
              `static/assets/fonts/bitmaps/${DEFAULT_FONT_NAME}.png`,
              `static/assets/fonts/bitmaps/${DEFAULT_FONT_NAME}.fnt`
          );
      }
      
      create() {
          game.physics.startSystem(Phaser.Physics.ARCADE);
          localforage.setItem(MAIN_PLAYER_DATA, {
              name: 'Biru',
              passedRusaTavern: false
          });
          game.state.start('loadGlobals');
      }
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */
  class AssetsLoader {
      loadJSON(loader) {
          loader.loadJSON();
      }
      loadAssets(loader) {
          loader.loadAssets();
      }
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  class ItemsLoader {
      loadJSON() {
          const itemList = game.cache.getJSON(MODS).items;
          itemList.map((item) => {
              const itemsRootPath = `${MODS_PATH}/items/${item}`;
              game.load.json(`item-${item}`, `${itemsRootPath}/item.json`);
          });
      }

      loadAssets() {
      }
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  class CharactersLoader {
      constructor() {
          this.characterList = game.cache.getJSON(MODS).characters;
      }

      loadJSON() {
          this.characterList.map((character) => {
              const placesRootPath = `${MODS_PATH}/characters/${character}`;
              game.load.json(`character-${character}`, `${placesRootPath}/character.json`);
          });
      }

      loadAssets() {
          // temporary hack, please
          // make a animatuon of 32x32 per frame!!!
          const pixelWidth = 41;
          const pixelHeight = 36;
          this.characterList.map((character) => {
              const placesRootPath = `${MODS_PATH}/characters/${character}`;
              const characterSpritePath = game.cache.getJSON(`character-${character}`).sprite;
              game.load.spritesheet(`character-${character}-sprite`, `${placesRootPath}/${characterSpritePath}`, pixelWidth, pixelHeight);
          });
      }
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  class PlacesLoader {
      constructor() {
          this.placeList = game.cache.getJSON(MODS).places;
      }
      
      loadJSON() {
          this.placeList.map((place) => {
              const placesRootPath = `${MODS_PATH}/places/${place}`;
              game.load.json(`place-${place}`, `${placesRootPath}/place.json`);
          });
      }

      loadAssets() {
          this.placeList.map((place) => {
              const texturesPath = game.cache.getJSON(`place-${place}`).textures;
              const mapPath = game.cache.getJSON(`place-${place}`).map;
              const placesRootPath = `${MODS_PATH}/places/${place}`;
              game.load.tilemap(`place-${place}-map`, `${placesRootPath}/${mapPath}`, null, Phaser.Tilemap.TILED_JSON);
              game.load.image(`place-${place}-textures`, `${placesRootPath}/${texturesPath}`);
          });
      }
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  /**
   * Preload all _GLOBAL_ game assets that aren't specific for Mods.
   *
   * Load games assets that are basically not covered in Mods
   * paths, whether items, characters, or places.
   */
  class LoadGlobals {
      preload() {
          const xPos = 0;
          const yPos = 0;
          game.add.bitmapText(xPos, yPos, DEFAULT_FONT_NAME, 'Loading global settings...');
          game.load.image('continueButtonUp', 'static/assets/buttons/continueButtonUp.png');
          game.load.image('menuBackgroundMimsi', 'static/assets/backgrounds/mimsi.png');
          game.load.image('dialogBoxBlue', 'static/assets/dialog-boxes/dialogBoxBlue.png');
          game.load.audio('musicBonfire', 'static/assets/music/bonfire.mp3');
          const bayuLoader = new AssetsLoader();
          bayuLoader.loadJSON(new ItemsLoader());
          bayuLoader.loadJSON(new CharactersLoader());
          bayuLoader.loadJSON(new PlacesLoader());
      }
      create() {
          // phaser-input type definitions are broken?
          // @ts-ignore
          game.add.plugin(PhaserInput.Plugin);
          game.state.start('loadAssets');
      }
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  class Inventory {
      create() {
          const headers = ['Name', 'Type'];
          const headerList = game.add.text(32, 64, '');
          headerList.parseList(headers);
          const items = [
              ['Iron Spear', 'Weapon'],
              ['Iron Shortsword', 'Weapon']
          ];
          const itemList = game.add.text(32, 120, '');
          itemList.parseList(items);
      }
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  /**
   * Put a text label right in the middle or center of a button.
   *
   * @param textLabel Text to be centered.
   * @param button Button to put the text into.
   */
  function centerTextWithinButton(textLabel, button) {
      const sizeOfButton = 2;
      textLabel.x = (button.width - textLabel.width) / sizeOfButton;
      textLabel.y = (button.height - textLabel.height) / sizeOfButton;
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  const assetsLoaderLabelYPos = 50;
  const defaultXPos = 0;
  const defaultYPos = 0;

  class LoadAssets {
      preload() {
          game.add.bitmapText(defaultXPos, defaultYPos, DEFAULT_FONT_NAME, 'Global settings loaded.');
          this.assetsLoaderLabel = game.add.bitmapText(
              defaultXPos,
              assetsLoaderLabelYPos,
              DEFAULT_FONT_NAME,
              'Loading assets...'
          );
          const bayuLoader = new AssetsLoader();
          bayuLoader.loadAssets(new CharactersLoader());
          bayuLoader.loadAssets(new PlacesLoader());
      }
      
      create() {
          this.assetsLoaderLabel.destroy();
          const fromTheRightOffset = 500;
          game.add.bitmapText(defaultXPos, assetsLoaderLabelYPos, DEFAULT_FONT_NAME, 'Assets loaded.');
          const continueToMenuButton = game.add.button(game.world.centerX, fromTheRightOffset, 'continueButtonUp', () => {
              game.state.start('menu');
          });
          const continueToMenuLabel = game.add.bitmapText(defaultXPos, 0, DEFAULT_FONT_NAME, 'Continue');
          continueToMenuButton.addChild(continueToMenuLabel);
          centerTextWithinButton(continueToMenuLabel, continueToMenuButton);
      }
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  /**
   * Fade the game screen with black color.
   *
   * @param seconds How many seconds until fade is complete.
   */
  function fadeToBlack(seconds) {
      const miliseconds = 1000;
      const blackScreen = 0x000000;
      game.camera.fade(blackScreen, (miliseconds * seconds));
  }

  /**
   * Run a new state/scene when fade process is complete.
   *
   * @param stateName State or scene name to start.
   * See StateEntries.[tj]s file for list of states.
   */
  function changeToStateOnFadeDone(stateName) {
      game.camera.onFadeComplete.add(function () {
          game.state.start(stateName);
      }, this);
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  /**
   * Game top-level menu screen.
   *
   * By default, starts storyOne scene if
   * 'Start' button is clicked.
   *
   * This is the scene that hosts three states.
   * 1. Start
   * 2. Settings
   * 3. Credits
   */
  class Menu {
      create() {
          const backgroundMusic = game.add.audio('musicBonfire');
          const defaultXPos = 0;
          const defaultYPos = 0;
          const secondsToComplete = 2;
          const startButtonYPos = 310;
          const creditsButtonYPos = 230;
          const xPosFromLeftOffset = 80;
          const versionLabelYPos = 500;
          const mimsiBackground = game.add.tileSprite(
              defaultXPos,
              defaultYPos,
              SCREEN_MAX_WIDTH,
              SCREEN_MAX_HEIGHT,
              'menuBackgroundMimsi'
          );
          // Start background music (infinitely looped);
          const volume = 0.6;
          backgroundMusic.loopFull(volume);
          const startButton = game.add.button(xPosFromLeftOffset, startButtonYPos, 'continueButtonUp', () => {
              backgroundMusic.stop();
              fadeToBlack(secondsToComplete);
          });
          const startLabel = game.add.bitmapText(defaultXPos, defaultYPos, DEFAULT_FONT_NAME, 'Start');
          startButton.addChild(startLabel);
          centerTextWithinButton(startLabel, startButton);
          changeToStateOnFadeDone('storyOne');
          const creditsButton = game.add.button(xPosFromLeftOffset, creditsButtonYPos, 'continueButtonUp', () => {
              backgroundMusic.stop();
              game.state.start('credits');
          });
          const creditsLabel = game.add.bitmapText(defaultXPos, defaultYPos, DEFAULT_FONT_NAME, 'Credits');
          creditsButton.addChild(creditsLabel);
          centerTextWithinButton(creditsLabel, creditsButton);
          mimsiBackground.visible;
          game.add.bitmapText(xPosFromLeftOffset, versionLabelYPos, DEFAULT_FONT_NAME, `v${VERSION}`);
      }
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  class Story {
      constructor(storyName) {
          this.storyName = storyName;
      }

      loadStory() {
          game.load.json(this.storyName, `static/assets/stories/${this.storyName}.json`);
      }

      rollStory() {
          const storyText = game.cache.getJSON(this.storyName).story;
          const wordDelay = game.cache.getJSON(this.storyName).wordDelay;
          const lineDelay = game.cache.getJSON(this.storyName).lineDelay;
          const defaultXPos = 0;
          const defaultYPos = 0;
          const textRenderer = game.add.bitmapText(defaultXPos, defaultYPos, DEFAULT_FONT_NAME, '');
          textRenderer.maxWidth = SCREEN_MAX_WIDTH;
          const storyTraverse = new StoryTraverse(storyText, textRenderer, wordDelay, lineDelay);
          storyTraverse.beginTraverseLine();
      }
  }

  class StoryTraverse {
      constructor(storyText, textRenderer, wordDelay, lineDelay) {
          this.line = [];
          this.wordIndex = 0;
          this.lineIndex = 0;
          this.storyText = storyText;
          this.textRenderer = textRenderer;
          this.wordDelay = wordDelay;
          this.lineDelay = lineDelay;
      }

      beginTraverseLine() {
          if (this.lineIndex === this.storyText.length) {
              return;
          }
          this.line = this.storyText[this.lineIndex].split(' ');
          this.wordIndex = 0;
          game.time.events.repeat(this.wordDelay, this.line.length, this.nextWord, this);
          this.lineIndex++;
      }

      nextWord() {
          this.textRenderer.text = this.textRenderer.text.concat(this.line[this.wordIndex] + ' ');
          this.wordIndex++;
          if (this.wordIndex === this.line.length) {
              this.textRenderer.text = this.textRenderer.text.concat('\n');
              game.time.events.add(this.lineDelay, this.beginTraverseLine, this);
          }
      }
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  class Credits {
      constructor() {
          this.bayuCredits = new Story('developersCredits');
      }
      
      preload() {
          this.bayuCredits.loadStory();
      }
      
      create() {
          this.bayuCredits.rollStory();
          const backToMenuButtonYPos = 500;
          const backToMenuButton = game.add.button(game.world.centerX, backToMenuButtonYPos, 'continueButtonUp', () => {
              game.state.start('menu');
          });
          const defaultXPos = 0;
          const defaultYPos = 0;
          const backLabel = game.add.bitmapText(defaultXPos, defaultYPos, DEFAULT_FONT_NAME, 'Back');
          backToMenuButton.addChild(backLabel);
          centerTextWithinButton(backLabel, backToMenuButton);
      }
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  class StoryOne {
      constructor() {
          this.bayuStory = new Story('storyOne');
      }

      preload() {
          this.bayuStory.loadStory();
      }

      create() {
          this.bayuStory.rollStory();
          const secondsToComplete = 2;
          const defaultXPos = 0;
          const defaultYPos = 0;
          const continueButtonYPos = 500;
          const continueButton = game.add.button(game.world.centerX, continueButtonYPos, 'continueButtonUp', () => {
              fadeToBlack(secondsToComplete);
          });
          const textLabel = game.add.bitmapText(defaultXPos, defaultYPos, DEFAULT_FONT_NAME, 'Continue');
          continueButton.addChild(textLabel);
          centerTextWithinButton(textLabel, continueButton);
          changeToStateOnFadeDone('placeRusaTavern');
      }
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  function makeCameraFollowCharacter(character) {
      game.camera.follow(character);
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */
  /**
   * Set character walking animations.
   * All exported animation keys for the character are:
   * idle, up, down, left, right.
   *
   * @parameter character Character object.
   * @parameter animations Animations object.
   * @parameter frameChangingspeed How fast the frame changing to another.
   */
  function setCharacterWalkingAnimations(character, animations, frameChangingSpeed = 10) {
      character.animations.add('idle', animations.idle, null, null);
      character.animations.add('up', animations.up, frameChangingSpeed);
      character.animations.add('down', animations.down, frameChangingSpeed, true);
      character.animations.add('left', animations.left, frameChangingSpeed, true);
      character.animations.add('right', animations.right, frameChangingSpeed, true);
  }
  function walkDirectionNorth(character, movementSpeed) {
      character.body.velocity.x = 0;
      character.body.velocity.y = -movementSpeed;
      character.animations.play('up');
  }
  function walkDirectionSouth(character, movementSpeed) {
      character.body.velocity.x = 0;
      character.body.velocity.y = movementSpeed;
      character.animations.play('down');
  }
  function walkDirectionWest(character, movementSpeed) {
      character.body.velocity.y = 0;
      character.body.velocity.x = -movementSpeed;
      character.animations.play('left');
  }
  function walkDirectionEast(character, movementSpeed) {
      character.body.velocity.y = 0;
      character.body.velocity.x = movementSpeed;
      character.animations.play('right');
  }
  function walkIdle(character) {
      character.body.velocity.y = 0;
      character.body.velocity.x = 0;
      character.animations.play('idle');
  }
  class Movement {
      constructor(character) {
          this.character = character;
      }
      enableMovement(characterJSONKey) {
          makeCameraFollowCharacter(this.character);
          const animations = game.cache.getJSON(characterJSONKey).animations;
          setCharacterWalkingAnimations(this.character, animations);
          this.key = game.input.keyboard;
          this.arrow = this.key.createCursorKeys();
      }
      makeMovableDesktop(movementSpeed) {
          if (this.key.isDown(Phaser.Keyboard.W) || this.arrow.up.isDown) {
              walkDirectionNorth(this.character, movementSpeed);
          }
          else if (this.key.isDown(Phaser.Keyboard.S) || this.arrow.down.isDown) {
              walkDirectionSouth(this.character, movementSpeed);
          }
          else if (this.key.isDown(Phaser.Keyboard.A) || this.arrow.left.isDown) {
              walkDirectionWest(this.character, movementSpeed);
          }
          else if (this.key.isDown(Phaser.Keyboard.D) || this.arrow.right.isDown) {
              walkDirectionEast(this.character, movementSpeed);
          }
          else {
              walkIdle(this.character);
          }
      }
      makeMovable(movementSpeed = 200) {
          this.makeMovableDesktop(movementSpeed);
      }
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>
    Copyright (C) 2019 by Teoh Han Hui <teohhanhui@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  function addDialog(x, y, dialogFrame) {
      let dialogBox;
      const onClick = new Promise((resolve) => {
          dialogBox = game.add.button(x, y, dialogFrame, () => {
              resolve();
          });
      });
      return { dialogBox, onClick };
  }

  class Dialog {
      display(playerName, text, dialogFrame = 'dialogBoxBlue') {
          const defaultXPos = 0;
          const dialogBoxYPos = 450;
          const { dialogBox, onClick } = addDialog(defaultXPos, dialogBoxYPos, dialogFrame);
          this.dialogBox = dialogBox;
          const playerLabelXPos = 20;
          const playerLabelYPos = 1;
          const playerLabel = game.add.bitmapText(playerLabelXPos, playerLabelYPos, DEFAULT_FONT_NAME, playerName);
          const textStoryXPos = 30;
          const textStoryYPos = 30;
          const textStory = game.add.bitmapText(textStoryXPos, textStoryYPos, DEFAULT_FONT_NAME, text);
          textStory.maxWidth = 800;
          this.dialogBox.fixedToCamera = true;
          this.dialogBox.addChild(playerLabel);
          this.dialogBox.addChild(textStory);
          return onClick;
      }

      delete() {
          this.dialogBox.destroy();
      }
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */
  function enableScrollDeltaForAllLayers(layers) {
      layers.map((layer) => {
          layer.renderSettings.enableScrollDelta = true;
      });
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>
    Copyright (C) 2019 by Teoh Han Hui <teohhanhui@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  class PlaceRusaTavern {
      create() {
          this.map = game.add.tilemap('place-rusa-tavern-map');
          this.map.addTilesetImage('rusa-tavern-textures', 'place-rusa-tavern-textures');
          this.backgroundLayer = this.map.createLayer('background-layer');
          this.furnitureLayer = this.map.createLayer('furniture-layer');
          this.decorationLayer = this.map.createLayer('decoration-layer');
          this.collisionLayer = this.map.createLayer('collision-layer');
          enableScrollDeltaForAllLayers([
              this.backgroundLayer,
              this.furnitureLayer,
              this.decorationLayer,
              this.collisionLayer
          ]);
          this.collisionLayer.visible = false;
          this.placeRaznokWestDoor = game.add.group();
          this.placeRaznokWestDoor.enableBody = true;
          const groupID = 61;
          const frame = 1;
          this.map.createFromObjects(
              'place-raznok-west-door-layer',
              groupID,
              'place-rusa-tavern-textures',
              frame,
              true,
              false,
              this.placeRaznokWestDoor
          );
          const fromMinID = 1;
          const toMaxID = 100000;
          this.map.setCollisionBetween(fromMinID, toMaxID, true, 'collision-layer');
          this.backgroundLayer.resizeWorld();
          this.mainPlayer = game.add.sprite(
              game.world.centerX,
              game.world.centerY,
              'character-biru-sprite'
          );
          game.physics.enable(this.mainPlayer, Phaser.Physics.ARCADE);
          this.mainPlayer.body.collideWorldBounds = true;
          this.bayuCharacterMovement = new Movement(this.mainPlayer);
          this.bayuCharacterMovement.enableMovement('character-biru');

          async function getMainPlayerData() {
              return localforage.getItem(MAIN_PLAYER_DATA);
          }
          
          getMainPlayerData().then((value) => {
              if (value.passedRusaTavern === false) {
                  localforage.setItem(MAIN_PLAYER_DATA, {
                      passedRusaTavern: true
                  });
                  const dialogBox = new Dialog();
                  dialogBox.display('You', 'Ugh...my head hurts...')
                      .then(() => dialogBox.delete())
                      .then(() => dialogBox.display('You', 'Where....am I?'))
                      .then(() => dialogBox.delete())
                      .then(() => dialogBox.display('Mysterious Figure', 'Nevermind me. Who are you?'))
                      .then(() => dialogBox.delete())
                      .then(() => dialogBox.display('You', 'Bleh'))
                      .then(() => dialogBox.delete());
              }
          });
      }
      
      update() {
          game.physics.arcade.collide(this.mainPlayer, this.collisionLayer);
          const goToPlaceRaznokWest = () => {
              game.state.start('placeRaznokWest');
          };
          game.physics.arcade.overlap(this.mainPlayer, this.placeRaznokWestDoor, goToPlaceRaznokWest);
          this.bayuCharacterMovement.makeMovable();
      } 
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */

  class PlaceRaznokWest {
      create() {
          this.map = game.add.tilemap('place-raznok-west-map');
          this.map.addTilesetImage('raznok-west-textures', 'place-raznok-west-textures');
          this.backgroundLayer = this.map.createLayer('background-layer');
          
          // Spawn player after backround layer has created. 
          this.mainPlayer = game.add.sprite(game.world.centerX, game.world.centerY, 'character-biru-sprite');
          this.decorationLayer = this.map.createLayer('decoration-layer');
          this.decorationTwoLayer = this.map.createLayer('decoration-two-layer');
          this.collisionLayer = this.map.createLayer('collision-layer');
          enableScrollDeltaForAllLayers([
              this.backgroundLayer,
              this.decorationLayer,
              this.decorationLayer,
              this.collisionLayer
          ]);
          this.placeRusaTavernDoor = game.add.group();
          this.placeRusaTavernDoor.enableBody = true;
          const groupID = 143;
          const frame = 1;
          this.map.createFromObjects(
              'place-rusa-tavern-door-layer',
              groupID,
              'place-raznok-west-textures',
              frame,
              true,
              false,
              this.placeRusaTavernDoor
          );
          
          this.collisionLayer.visible = false;
          const fromMinID = 1;
          const toMaxID = 100000;
          this.map.setCollisionBetween(fromMinID, toMaxID, true, 'collision-layer');
          this.backgroundLayer.resizeWorld();
          game.physics.enable(this.mainPlayer, Phaser.Physics.ARCADE);
          this.mainPlayer.body.collideWorldBounds = true;
          this.bayuCharacterMovement = new Movement(this.mainPlayer);
          this.bayuCharacterMovement.enableMovement('character-biru');
      }
      
      update() {
          game.physics.arcade.collide(this.mainPlayer, this.collisionLayer);
          const goToPlaceRusaTavern = () => {
              game.state.start('placeRusaTavern');
          };
          game.physics.arcade.overlap(this.mainPlayer, this.placeRusaTavernDoor, goToPlaceRusaTavern);
          this.bayuCharacterMovement.makeMovable();
      }
  }

  /*
    This file is part of Bayu of Chersonesse.
    Copyright (C) 2019 by Momozor <skelic3@gmail.com>

    Bayu of Chersonesse is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Bayu of Chersonesse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.
  */
  // Register all game scenes here.
  game.state.add('boot', Boot);
  game.state.add('loadGlobals', LoadGlobals);
  game.state.add('inventory', Inventory);
  game.state.add('loadAssets', LoadAssets);
  game.state.add('menu', Menu);
  game.state.add('credits', Credits);
  game.state.add('storyOne', StoryOne);
  game.state.add('placeRusaTavern', PlaceRusaTavern);
  game.state.add('placeRaznokWest', PlaceRaznokWest);
  // Entry point.
  game.state.start('boot');

}(localforage));
