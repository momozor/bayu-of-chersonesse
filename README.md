[![Build Status](https://travis-ci.org/momozor/bayu-of-chersonesse.svg?branch=master)](https://travis-ci.org/momozor/bayu-of-chersonesse)
# Bayu of Chersonesse
A 2D cross cultural rpg fantasy on the web!

Bayu of Chersonesse is a singleplayer RPG game that took place in a fantasy world called Mimsi, with
different kind of living beings, lores and magic.

Complete your quests and venture the world now!

## Supported Platforms
* Currently web browsers that supports ES6/ES7 features.
* Platform with keyboard access.

## Contributing
TODO

## License
Bayu of Chersonesse is free software.
This project is released under the AGPL-3.0 license. Please see LICENSE file for more details.
